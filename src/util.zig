const std = @import("std");
const gpu = @import("gpu");
const glfw = @import("glfw");
const ps = std.process;
const log = std.log;
const mem = std.mem;

const Config = struct {
    window_title: [*:0]const u8 = "Hello WGPU",
    window_width: u32 = 800,
    window_height: u32 = 600,
    key_callback: ?KeyCallback = null,
};

const Context = struct {
    const Self = @This();

    ally: mem.Allocator,
    window: glfw.Window,
    instance: *gpu.Instance,
    adapter: *gpu.Adapter,
    device: *gpu.Device,
    queue: *gpu.Queue,

    pub fn getSwapChain(self: Self) ?*gpu.SwapChain {
        const size = self.window.getFramebufferSize();
        const data = self.window.getUserPointer(SurfaceData) orelse return null;

        data.target_desc.?.width = size.width;
        data.target_desc.?.height = size.height;

        const has_resized = !std.meta.eql(data.current_desc, data.target_desc);

        if (data.swap_chain == null or has_resized) {
            data.swap_chain = self.device.createSwapChain(
                data.surface.?,
                &data.target_desc.?,
            );

            data.current_desc = data.target_desc;
        }

        return data.swap_chain;
    }

    pub fn deinit(self: Self) void {
        var data = self.window.getUserPointer(SurfaceData);
        if (data) |d| self.ally.destroy(d);
        self.window.destroy();
        glfw.terminate();
    }
};

pub fn setup(ally: mem.Allocator, comptime cfg: Config) Context {
    // Initialize GPU
    gpu.Impl.init();

    // Initialize GLFW
    const success = glfw.init(.{ .platform = .wayland });
    // const success = glfw.init(.{});

    if (!success) {
        log.err("Failed to initialize GLFW: {?s}\n", .{glfw.getErrorString()});
        ps.exit(1);
    }

    glfw.setErrorCallback(errorCallback);

    log.info("GPU and GLFW initialized successfully", .{});

    // Initialize Window
    const win = glfw.Window.create(
        cfg.window_width,
        cfg.window_height,
        cfg.window_title,
        null,
        null,
        .{ .client_api = .no_api },
    ) orelse {
        log.err("Failed to create Window: {?s}\n", .{glfw.getErrorString()});
        ps.exit(1);
    };

    win.setKeyCallback(cfg.key_callback);
    // win.setFramebufferSizeCallback(handleResize);

    // Get Instance
    const instance = gpu.createInstance(null) orelse {
        log.err("Failed to create GPU instance \n", .{});
        ps.exit(1);
    };

    // Get Surface
    // const glfw_native = glfw.Native(.{ .x11 = true });
    // const surface_desc: gpu.Surface.Descriptor = .{
    //     .next_in_chain = .{
    //         .from_xlib_window = &.{
    //             .display = glfw_native.getX11Display(),
    //             .window = glfw_native.getX11Window(win),
    //         },
    //     },
    // };

    const glfw_native = glfw.Native(.{ .wayland = true });
    const surface_desc: gpu.Surface.Descriptor = .{
        .next_in_chain = .{
            .from_wayland_surface = &.{
                .display = glfw_native.getWaylandDisplay(),
                .surface = glfw_native.getWaylandWindow(win),
            },
        },
    };

    const surface = instance.createSurface(&surface_desc);

    // Get Adapter

    const adapter_opts: gpu.RequestAdapterOptions = .{
        .compatible_surface = surface,
        .power_preference = .undefined,
        .force_fallback_adapter = false,
    };

    var response: ?RequestAdapterResponse = null;
    instance.requestAdapter(&adapter_opts, &response, requestAdapterCallback);

    if (response.?.status != .success) {
        std.log.err("Could not get Adapter: {s}\n", .{response.?.message.?});
        ps.exit(1);
    }

    const adapter = response.?.adapter;

    var props = std.mem.zeroes(gpu.Adapter.Properties);
    adapter.getProperties(&props);

    std.log.info("found {s} backend on {s}", .{
        props.backend_type.name(),
        props.adapter_type.name(),
    });

    std.log.info("adapter = {s}, {s}", .{
        props.name,
        props.driver_description,
    });

    // Create Device

    const device = adapter.createDevice(null) orelse {
        std.log.err("Failed to create GPU device.\n", .{});
        ps.exit(1);
    };

    device.setUncapturedErrorCallback({}, printUnhandledErrorCallback);

    // Get Queue
    const queue = device.getQueue();

    // Get SwapChain

    const size = win.getFramebufferSize();

    const swap_desc: gpu.SwapChain.Descriptor = .{
        .label = "basic swap chain",
        .usage = .{ .render_attachment = true },
        .format = .bgra8_unorm,
        .width = size.width,
        .height = size.height,
        .present_mode = .fifo,
    };

    const data = ally.create(SurfaceData) catch |err| {
        log.err("Memory Error [{}]", .{err});
        ps.exit(1);
    };

    data.* = .{
        .surface = surface,
        .current_desc = swap_desc,
        .target_desc = swap_desc,
        .swap_chain = device.createSwapChain(surface, &swap_desc),
    };

    win.setUserPointer(data);

    return .{
        .ally = ally,
        .window = win,
        .instance = instance,
        .adapter = adapter,
        .device = device,
        .queue = queue,
    };
}

// Custom Types

const SurfaceData = struct {
    surface: ?*gpu.Surface,
    swap_chain: ?*gpu.SwapChain,
    current_desc: ?gpu.SwapChain.Descriptor,
    target_desc: ?gpu.SwapChain.Descriptor,
};

const RequestAdapterResponse = struct {
    status: gpu.RequestAdapterStatus,
    adapter: *gpu.Adapter,
    message: ?[*:0]const u8,
};

const KeyCallback = fn (
    window: glfw.Window,
    key: glfw.Key,
    scancode: i32,
    action: glfw.Action,
    mods: glfw.Mods,
) void;

// GLFW Callabacks

fn handleResize(win: glfw.Window, width: u32, height: u32) void {
    var pl = win.getUserPointer(SurfaceData).?;
    pl.target_desc.?.width = width;
    pl.target_desc.?.height = height;
}

// Error Callbacks

fn errorCallback(code: glfw.ErrorCode, desc: [:0]const u8) void {
    log.err("GLFW Error [{}] : {s}\n", .{ code, desc });
}

inline fn requestAdapterCallback(
    context: *?RequestAdapterResponse,
    status: gpu.RequestAdapterStatus,
    adapter: *gpu.Adapter,
    message: ?[*:0]const u8,
) void {
    context.* = RequestAdapterResponse{
        .adapter = adapter,
        .status = status,
        .message = message,
    };
}

inline fn printUnhandledErrorCallback(
    _: void,
    typ: gpu.ErrorType,
    message: [*:0]const u8,
) void {
    switch (typ) {
        .validation => log.err("gpu: validation error: {s}\n", .{message}),
        .out_of_memory => log.err("gpu: out_of_memory error: {s}\n", .{message}),
        .device_lost => log.err("gpu: device_lost error: {s}\n", .{message}),
        .unknown => log.err("gpu: unknown error: {s}\n", .{message}),
        else => unreachable,
    }
    ps.exit(1);
}
