struct Uniform {
    time_ms: f32,    
    height: f32,
    width: f32,
}

@group(0) @binding(0) var<uniform> uni: Uniform;

struct VertexOutput {
    @builtin(position) pos: vec4<f32>,
}

@vertex 
fn vs_main( @builtin(vertex_index) in: u32 ) -> VertexOutput { 
    var out: VertexOutput;

    var pos = array<vec2<f32>, 6>( 
        vec2<f32>( -1.0,  1.0), 
        vec2<f32>( -1.0, -1.0),
        vec2<f32>(  1.0,  1.0), 
        vec2<f32>( -1.0, -1.0),
        vec2<f32>(  1.0, -1.0),
        vec2<f32>(  1.0,  1.0),
    ); 

    out.pos = vec4<f32>(pos[in], 0.0, 1.0); 
    return out;
} 

@fragment 
fn fs_main(in: VertexOutput) -> @location(0) vec4<f32> { 
    return vec4<f32>(0.9, 0.6, 0.2, 1.0); 
}
