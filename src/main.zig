const std = @import("std");
const gpu = @import("gpu");
const glfw = @import("glfw");
const util = @import("util.zig");
const time = std.time;

pub const GPUInterface = gpu.dawn.Interface;

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const ally = gpa.allocator();

    const ctx = util.setup(ally, .{
        .window_title = "Hello Triangle",
        .key_callback = handleKeyPress,
    });

    defer ctx.deinit();

    // Configure shaders
    const shader = @embedFile("shader.wgsl");
    const shader_module = ctx.device.createShaderModuleWGSL("default shader", shader);
    defer shader_module.release();

    const fragment = gpu.FragmentState.init(.{
        .module = shader_module,
        .entry_point = "fs_main",
        .targets = &.{.{
            .format = .bgra8_unorm,
            .blend = &gpu.BlendState{
                .color = .{ .dst_factor = .one },
                .alpha = .{ .dst_factor = .one },
            },
            .write_mask = gpu.ColorWriteMaskFlags.all,
        }},
    });

    // Create Buffers

    const uniform_buffer = ctx.device.createBuffer(&.{
        .label = "Uniform Buffer",
        .usage = .{
            .uniform = true,
            .copy_dst = true,
        },
        .size = @sizeOf(Uniform),
    });

    // Configure Bind grouops

    const uniform_layout_desc = gpu.BindGroupLayout.Descriptor.init(.{
        .label = "Uniform bind group layout",
        .entries = &[_]gpu.BindGroupLayout.Entry{.{
            .binding = 0,
            .visibility = .{
                .fragment = true,
            },
            .buffer = .{
                .type = .uniform,
                .has_dynamic_offset = false,
                .min_binding_size = 0,
            },
        }},
    });

    const uniform_layout = ctx.device.createBindGroupLayout(&uniform_layout_desc);

    const uniform_bind_desc = gpu.BindGroup.Descriptor.init(.{
        .label = "Uniform bind group",
        .layout = uniform_layout,
        .entries = &[_]gpu.BindGroup.Entry{.{
            .binding = 0,
            .buffer = uniform_buffer,
            .size = @sizeOf(Uniform),
        }},
    });

    const uniform_bind_group = ctx.device.createBindGroup(&uniform_bind_desc);

    // Configure Render pipeline

    const layout_desc = gpu.PipelineLayout.Descriptor.init(.{
        .label = "Render Pipeline Layout",
        .bind_group_layouts = &[_]*gpu.BindGroupLayout{uniform_layout},
    });

    const layout = ctx.device.createPipelineLayout(&layout_desc);

    const pipeline_descriptor = gpu.RenderPipeline.Descriptor{
        .label = "Render pipeline",
        .layout = layout,
        .fragment = &fragment,
        .depth_stencil = null,
        .vertex = gpu.VertexState{
            .module = shader_module,
            .entry_point = "vs_main",
        },
        .multisample = .{},
        .primitive = .{ .cull_mode = .back },
    };

    const pipeline = ctx.device.createRenderPipeline(&pipeline_descriptor);

    // Event loop

    var time_ms: u32 = 0;

    while (!ctx.window.shouldClose()) {
        glfw.pollEvents();

        const size = ctx.window.getFramebufferSize();

        ctx.queue.writeBuffer(uniform_buffer, 0, &[_]Uniform{.{
            .width = @intToFloat(f32, size.width),
            .height = @intToFloat(f32, size.height),
            .time = @intToFloat(f32, time_ms),
        }});

        const swap_chain = ctx.getSwapChain().?;

        const buffer_view = swap_chain.getCurrentTextureView();
        defer buffer_view.?.release();

        const color_attachment = gpu.RenderPassColorAttachment{
            .view = buffer_view,
            .clear_value = gpu.Color{
                .r = 0.0,
                .g = 0.0,
                .b = 0.0,
                .a = 1.0,
            },
            .load_op = .clear,
            .store_op = .store,
        };

        const render_pass_info = gpu.RenderPassDescriptor.init(.{
            .color_attachments = &.{color_attachment},
        });

        const encoder = ctx.device.createCommandEncoder(null);
        defer encoder.release();

        const pass = encoder.beginRenderPass(&render_pass_info);
        defer pass.release();

        pass.setPipeline(pipeline);
        pass.setBindGroup(0, uniform_bind_group, null);
        pass.draw(6, 1, 0, 0);
        pass.end();

        var command = encoder.finish(null);
        defer command.release();

        ctx.queue.submit(&[_]*gpu.CommandBuffer{command});
        swap_chain.present();

        time.sleep(16 * time.ns_per_ms);
        time_ms +%= 16;
    }
}

fn handleKeyPress(
    win: glfw.Window,
    key: glfw.Key,
    scancode: i32,
    action: glfw.Action,
    mods: glfw.Mods,
) void {
    if (key == .q) win.setShouldClose(true);
    _ = mods;
    _ = action;
    _ = scancode;
}

const Uniform = struct {
    time: f32,
    width: f32,
    height: f32,
};
